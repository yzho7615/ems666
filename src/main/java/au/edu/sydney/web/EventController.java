package au.edu.sydney.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.MediaEventPair;
<<<<<<< HEAD
import au.edu.sydney.domain.User;
=======
>>>>>>> 970794cd934f30393eec0fb28338b484916b8f56
import au.edu.sydney.service.CategoryManager;
import au.edu.sydney.service.EventManager;
import au.edu.sydney.service.MediaManager;
import au.edu.sydney.utils.DateUtil;

@Controller
@RequestMapping(value = "/event")
public class EventController {

	@Autowired
	private EventManager eventManager;

	@Autowired
	private CategoryManager categoryManager;

	@Autowired
	private MediaManager mediaManager;

	@RequestMapping(value = "/post", method = RequestMethod.GET)
	public String viewPost(Map<String, Object> model, HttpServletRequest request) {
		Event eventForm = new Event();
		model.put("eventForm", eventForm);

		model.put("categoryNames", categoryManager.getCategoryNames());

		return "event_post";
	}

	@RequestMapping(value = "/post", method = RequestMethod.POST)
	public String processPost(@ModelAttribute("eventForm") Event event, HttpServletRequest request) {

		if (eventManager.postEvent(event)) {
			request.setAttribute("eventId",
					eventManager.retrieveEvent(event.getOrganizer(), event.getEventName()).getEventId());
			return "event_post_success";
		}

		return "event_post_failure";

	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewEvent(HttpServletRequest request) {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("eventId"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (eventId != 0) {
			Event event = eventManager.retrieveEvent(eventId);

			event.setStartDate(DateUtil.toYMDHMS(event.getStartDate()));
			event.setEndDate(DateUtil.toYMDHMS(event.getEndDate()));

			MediaEventPair mediaEventPair = new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()), mediaManager.getEventAudios(event.getEventId()));
			request.setAttribute("mediaEventPair", mediaEventPair);

			return "event_home";
		}

		return null;
	}

	@RequestMapping(value = "/category", method = RequestMethod.GET)
	public String viewEventsByCategory(HttpServletRequest request) {
		String category = request.getParameter("category");
		
		if (category!=null)	{
			List<Event> events = eventManager.retrieveEventsByCategory(category);
			List<MediaEventPair> mediaEventPairs = new ArrayList<MediaEventPair>();
			for (Iterator<Event> iterator=events.iterator(); iterator.hasNext();) {
				Event event = iterator.next();
				mediaEventPairs.add(new MediaEventPair(event, mediaManager.getEventImages(event.getEventId()), mediaManager.getEventAudios(event.getEventId())));
			}
			request.setAttribute("category", category);
			request.setAttribute("mediaEventPairs", mediaEventPairs);
			return "events_by_category";
		}
		
		return null;
	}
<<<<<<< HEAD
	
	//show my posts
	@RequestMapping(value = "/myPosts", method = RequestMethod.GET)
	public String viewEvent(Map<String, Object> model, HttpServletRequest request) {
		String username = request.getParameter("username");

		if (username != null) {

			List<Event> events = eventManager.retrieveEventsByOrganizer(username);

			if (!events.isEmpty()) {
				request.setAttribute("events", events);
			}

			return "my_posts";
		}

		return null;
	}
	
	
	//*******************delete events
	@RequestMapping(value="/deleteEventById", method=RequestMethod.GET)
	public String deleteEventsById(HttpServletRequest request){
		String eventId = request.getParameter("eventId");
		if (eventId != null) {
			int id = 0;
			try {
				id = Integer.parseInt(eventId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (id != 0) {
				eventManager.deleteEventsById(id);
				return "redirect:./myPosts?username=" + ((User) request.getSession().getAttribute("user")).getUsername();
			}
		}
		return null;
	}
	
	//**********************edit post****get past info of current event
	@RequestMapping(value = "/editPost", method = RequestMethod.GET)
	public String editPost(HttpServletRequest request) {
		String eventId = request.getParameter("eventId");
		if (eventId != null) {
			int id = 0;
			try {
				id = Integer.parseInt(eventId);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (id != 0) {
				Event eventEdit  = eventManager.retrieveEvent(id);
				if (eventEdit != null) {
					Event eventForm = new Event();
					request.setAttribute("eventForm", eventEdit);
					request.setAttribute("eventEdit", eventEdit);
					request.setAttribute("categoryNames", categoryManager.getCategoryNames());
					return "event_edit";
				}
			}
		}
		return null;
	}
	//***edit post**** update modified event
	@RequestMapping(value = "/editPost", method = RequestMethod.POST)
	public String updatesPost(@ModelAttribute("eventForm") Event event, HttpServletRequest request) {
		if(event != null) {
			eventManager.updateEvents(event);
			return "event_update_success";
		}
		return "event_update_failure";
	}

=======
>>>>>>> 970794cd934f30393eec0fb28338b484916b8f56
}
