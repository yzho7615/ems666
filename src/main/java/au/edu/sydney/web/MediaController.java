package au.edu.sydney.web;

import java.io.File;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import au.edu.sydney.domain.EventAudio;
import au.edu.sydney.domain.EventImage;
import au.edu.sydney.service.MediaManager;

@Controller
@RequestMapping(value = "/media")
public class MediaController {
	@Autowired
	private MediaManager mediaManager;

	@RequestMapping(value = "/upload", method = RequestMethod.GET)
	public String viewUpload(HttpServletRequest request) {
		request.setAttribute("eventId", request.getParameter("eventId"));
		return "media_upload";
	}

	@RequestMapping(value = "/upload", method = RequestMethod.POST)
	public String processUpload(@RequestParam(value = "file", required = false) MultipartFile[] files,
			HttpServletRequest request) throws Exception {
		int eventId = 0;
		try {
			eventId = Integer.parseInt(request.getParameter("eventId"));
		} catch (Exception e) {
			e.printStackTrace();
		}

		String pathRoot = request.getSession().getServletContext().getRealPath("");
		String path = "";

		EventImage eventImage = new EventImage();
		EventAudio eventAudio = new EventAudio();

		if (eventId != 0) {
			System.out.println(eventId);
			for (MultipartFile file : files) {
				if (!file.isEmpty()) {
					String uuid = UUID.randomUUID().toString().replaceAll("-", "");
					String contentType = file.getContentType();
					System.out.println(contentType);
					String fileName = contentType.substring(contentType.indexOf("/") + 1);
					
					if (fileName.equals("mpeg")) {
						fileName = "mp3";
					}

					if (contentType.contains("image")) {
						path = "/static_resources/images/" + uuid + "." + fileName;
						eventImage.setEventId(eventId);
						eventImage.setImageUrl(path);
					}
					if (contentType.contains("audio")) {
						path = "/static_resources/audios/" + uuid + "." + fileName;
						eventAudio.setEventId(eventId);
						eventAudio.setAudioUrl(path);
					}
					System.out.println(path);
					file.transferTo(new File(pathRoot + path));
				}
			}
		}

		boolean imageSaved = mediaManager.addEventImage(eventImage);
		boolean audioSaved = mediaManager.addEventAudio(eventAudio);

		if (imageSaved || audioSaved) {
			request.setAttribute("eventId", request.getParameter("eventId"));
			return "media_upload_success";
		}

		return "media_upload_failure";

	}
}
