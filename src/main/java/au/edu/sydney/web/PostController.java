package au.edu.sydney.web;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import au.edu.sydney.domain.Event;
import au.edu.sydney.service.EventManager;

@Controller
@RequestMapping(value = "/post")
public class PostController {

	@Autowired
	private EventManager eventManager;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String viewEvent(Map<String, Object> model, HttpServletRequest request) {
		String username = request.getParameter("username");

		if (username != null) {

			List<Event> events = eventManager.retrieveEventsByOrganizer(username);

			if (!events.isEmpty()) {
				request.setAttribute("events", events);
			}

			return "my_posts";
		}

		return null;
	}

}
